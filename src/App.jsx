import React from 'react';
// import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import TodoList from './components/TodoList';

class App extends React.Component {
  state = {
    todoData: [
      {
        title: 'Javascript',
        description: 'Front End | Full Stack'
      },
      {
        title: 'Typescript',
        description: 'Front End | Full Stack'
      },
      {
        title: 'Python',
        description: 'Web | A.I. | Full Stack'
      }
    ]
  };

  deleteItem = (title) => {
    this.setState({ todoData: [...this.state.todoData.filter(todo => todo.title !== title)] });
  }

  addItem = (data) => {
    let dataIsNew = !this.state.todoData.find(todo => todo.title === data.title);
    if (dataIsNew) {
      this.state.todoData.push(data);
      this.setState({todoData: this.state.todoData});
    }
  }

  render() {
    return (
      <React.Fragment>
        <Header onInsert={this.addItem}/>
        <TodoList data={this.state.todoData} onDelete={this.deleteItem} />
      </React.Fragment>
    );
  }
}

export default App;
