import React from 'react';

export default class DataForm extends React.Component {
    state = {
        title: '',
        description: ''
    }

    add() {}

    update = (e) => {
        e.preventDefault();
        this.setState({title: e.currentTarget.title.value, description: ''});
        this.props.onInsert(this.state);
    }

    render() {
        return (
            <form onSubmit={this.update}>
                <input name="title"
                    onChange={(e) => this.setState({title: e.currentTarget.value, description: ''})}
                    value={this.state.title}
                    placeholder="Add new Item ..."
                />
                <input type="submit" value="Add"/>
            </form>
        );
    }
}