import React from 'react';
import DataForm from './DataForm'

export default class Header extends React.Component {
    render() {
        return (
            <header>
                <h1>ToDo</h1>
                <nav></nav>
                <DataForm onInsert={this.props.onInsert}></DataForm>
            </header>
        );
    }
}