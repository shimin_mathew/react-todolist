import React from 'react';
import TodoListItem from './TodoListItem'

export default class TodoList extends React.Component{
    // data, onDelete
    render() {
        if (this.props.data.length === 0)
            return <p>Add items to the list</p>
        return <div className="todolist">
            {this.props.data.map(todo => 
                <TodoListItem key={todo.title} item={todo} onDelete={this.props.onDelete}/>
            )}
            </div>;
    }
}