import React from 'react';

export default class TodoListItem extends React.Component{

    style = {
        backgroundColor: '#bfbf36',
        padding: '1em 2em'
    };

    render() {
        let {title, description} = this.props.item;
        return (
                <div style={this.style} className="todolist-item">
                    <h3>{title}</h3>
                    <p>{description}</p>
                    <button onClick={e => this.props.onDelete(title)}>Delete</button>
                </div>
        );
    }
}